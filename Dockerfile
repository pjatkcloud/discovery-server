# obraz bazowy dla aplikacji
FROM openjdk:17-jdk-slim

# katalog roboczy
WORKDIR /app

# plik JAR do obrazu Dockera
COPY build/libs/*.jar /app/

# punkt wejścia, aby uruchomić aplikację
ENTRYPOINT ["java", "-Djava.version=17", "-jar", "discovery-server-0.0.1-SNAPSHOT.jar"]

# Ustawienie tagu
LABEL tag="1.0.0"

EXPOSE 8761

#docker build -t discovery-server:1.0.0 .